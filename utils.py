import csv


def log_operation(func):
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        with open("file.csv", "w+") as f:
            writer = csv.writer(f)
            writer.writerow(["success", "exchanged_currency", "exchanged_amount", "rate", "balance"])
            writer.writerow([result.success, result.exchanged_currency, result.exchanged_amount, result.rate, result.balance])
        return result
    return wrapper
