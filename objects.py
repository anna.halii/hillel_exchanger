import requests
from collections import namedtuple
from utils import log_operation


class Exchanger:
    def __init__(self, amount_uah, amount_usd):
        self.amount_uah = amount_uah
        self.amount_usd = amount_usd
        self.privat_course = {}
        self.update_current_course()

    def update_current_course(self):
        response = requests.get("https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5").json()
        for item in response:
            self.privat_course[item["ccy"].lower()] = {"buy": float(item["buy"]), "sale": float(item["sale"])}

        self.privat_course["uah"] = {"buy": float(1 / self.privat_course["usd"]["buy"]),
                                     "sale": float(1 / self.privat_course["usd"]["sale"])}

        self.privat_course['balance'] = {"uah": self.amount_uah, "usd": self.amount_usd}

    def show_exchange_course(self, chosen_currency):
        if chosen_currency.lower() not in ("btc", "eur", "uah", "usd"):
            return print(f"Please, select a currency from these: BTC, EUR, UAH, USD")

        return print(f"RATE : {self.privat_course[chosen_currency.lower()]['sale']}, "
                     f"AVAILABLE UAH : {self.privat_course['balance']['uah']}, "
                     f"USD : {self.privat_course['balance']['usd']}")

    @log_operation
    def exchange_money(self, chosen_currency, amount):
        if chosen_currency.lower() not in ('usd', 'uah'):
            return print("Sorry, but temporary we supports only USD <-> UAH operations")

        Result = namedtuple('Result', ["success", "exchanged_currency", "exchanged_amount", "rate", "balance"])
        exchanged_currency = "uah" if chosen_currency.lower() == "usd" else "usd"
        balance = self.privat_course["balance"][exchanged_currency]
        rate = self.privat_course[chosen_currency.lower()]["sale"]
        exchanged_amount = amount * rate

        if exchanged_amount > balance:
            return Result(False, exchanged_currency, exchanged_amount, rate, balance)

        self.privat_course["balance"][exchanged_currency] -= exchanged_amount
        self.privat_course["balance"][currency] += amount

        print(f"{exchanged_currency.upper()} : {exchanged_amount}, RATE : {rate}")
        return Result(True, exchanged_currency, exchanged_amount, rate, self.privat_course["balance"][exchanged_currency])