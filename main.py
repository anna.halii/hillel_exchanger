import sys
from objects import Exchanger


if __name__ == '__main__':
    exc = Exchanger(10000.00, 5000.00)

    while True:
        print("\n 1 : COURSE \n 2 : EXCHANGE \n 3 : STOP")
        customer_choice = input("Please, choose your action")
        if customer_choice == "1":
            currency = input("Input your currency:")
            exc.show_exchange_course(currency)
        elif customer_choice == "2":
            currency = input("Input your currency:")
            amount = input("Input your amount:")
            exc.exchange_money(currency, float(amount))
        elif customer_choice == "3":
            print("SERVICE STOPPED")
            sys.exit(0)
        else:
            print("INVALID INPUT.")
            sys.exit(0)
